def format_input(file_name: str) -> list[list[str]]:
    grid = open(file_name).read()
    return [[ char for char in row] for row in grid.strip().split("\n")]

def is_word(row: int,col: int,grid: list[list[str]]) -> bool:
    if col < len(grid) - 1:
        before = '#' if col == 0 else grid[row][col - 1]
        after = grid[row][col + 1]
        if before == '#' and after == '_':
            return True

    if row < len(grid) - 1:
        top = '#' if row == 0 else grid[row - 1][col]
        bottom = grid[row + 1][col]
        if top == '#' and bottom == '_':
            return True
    return False    

def is_clue(row: int,col: int,grid: list[list[str]]) -> bool:
    
    def check_possibility(indexA: int, indexB: int) -> bool:
        if indexB < len(grid) - 1:
            before = '#' if indexB == 0 else grid[indexA][indexB - 1]
            after = grid[indexA][indexB + 1]
            if before == '#' and after == '_':
                return True

    return check_possibility(row, col) or check_possibility(col, row)

def number_crossword(file_name: str) -> list:
    clue = 0
    output = []
    grid = format_input(file_name)

    for row_pos, row in enumerate(grid):
        for col_pos, el in enumerate(row):
            if el == "_" and is_word(row_pos, col_pos, grid):
                clue += 1
                output.append((row_pos, col_pos, clue))
    
    return output


print(number_crossword("input.txt"))
